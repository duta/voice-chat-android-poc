package com.example.mumbleclient;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.example.mumbleclient.service.BaseServiceObserver;
import com.example.mumbleclient.service.IServiceObserver;
import com.example.mumbleclient.service.MumbleService;

import org.junit.Assert;

public class MainActivity extends AppCompatActivity {
    private IServiceObserver mObserver;

    private MumbleService mService;

    ServiceConnection mServiceConn = new ServiceConnection() {
        public void onServiceConnected(
                final ComponentName className,
                final IBinder binder) {
            mService = ((MumbleService.LocalBinder) binder).getService();
            Log.i("Mumble", "mService set");
            registerConnectionReceiver();
        }

        public void onServiceDisconnected(final ComponentName arg0) {
            mService = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, MumbleService.class);
        bindService(intent, mServiceConn, Context.BIND_AUTO_CREATE);
    }

    public void onLogin(View view) {

        final String host = ((EditText)findViewById(R.id.serverText)).getText().toString();
        final int port = Integer.parseInt(((EditText)findViewById(R.id.portText)).getText().toString());
        final String username = ((EditText)findViewById(R.id.nameText)).getText().toString();
        final String password = ((EditText)findViewById(R.id.passText)).getText().toString();

        registerConnectionReceiver();

        final Intent connectionIntent = new Intent(this, MumbleService.class);
        connectionIntent.setAction(MumbleService.ACTION_CONNECT);
        connectionIntent.putExtra(MumbleService.EXTRA_HOST, host);
        connectionIntent.putExtra(MumbleService.EXTRA_PORT, port);
        connectionIntent.putExtra(MumbleService.EXTRA_USERNAME, username);
        connectionIntent.putExtra(MumbleService.EXTRA_PASSWORD, password);
        startService(connectionIntent);
    }

    private class ServerServiceObserver extends BaseServiceObserver {
        @Override
        public void onConnectionStateChanged(final int state) {
            checkConnectionState();
        }
    }

    private final boolean checkConnectionState() {
        switch (mService.getConnectionState()) {
            case MumbleService.CONNECTION_STATE_CONNECTING:
                //show progress bar or something
                break;
            case MumbleService.CONNECTION_STATE_SYNCHRONIZING:
                //show progress bar or something
                break;
            case MumbleService.CONNECTION_STATE_CONNECTED:
                unregisterConnectionReceiver();
                final Intent i = new Intent(this, ChannelListActivity.class);
                startActivityForResult(i, 1);
                return true;
            case MumbleService.CONNECTION_STATE_DISCONNECTED:
                Log.i(Globals.LOG_TAG, "ServerList: Disconnected");
                break;
            default:
                Assert.fail("Unknown connection state");
        }

        return false;
    }

    private void registerConnectionReceiver() {
        if (mObserver != null) {
            return;
        }

        mObserver = new ServerServiceObserver();

        if (mService != null) {
            mService.registerObserver(mObserver);
        }
    }

    private void unregisterConnectionReceiver() {
        if (mObserver == null) {
            return;
        }

        if (mService != null) {
            mService.unregisterObserver(mObserver);
        }

        mObserver = null;
    }
}
