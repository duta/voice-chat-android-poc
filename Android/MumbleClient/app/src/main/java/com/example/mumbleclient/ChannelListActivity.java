package com.example.mumbleclient;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.mumbleclient.service.BaseServiceObserver;
import com.example.mumbleclient.service.IServiceObserver;
import com.example.mumbleclient.service.MumbleService;
import com.example.mumbleclient.service.model.Channel;

import java.util.List;

public class ChannelListActivity extends AppCompatActivity {

    private MumbleService mService;

    ServiceConnection mServiceConn = new ServiceConnection() {
        public void onServiceConnected(
                final ComponentName className,
                final IBinder binder) {
            mService = ((MumbleService.LocalBinder) binder).getService();
            updateListView();
            mService.registerObserver(mObserver);
        }

        public void onServiceDisconnected(final ComponentName arg0) {
            mService = null;
            ((ListView) findViewById(R.id.channelList)).setAdapter(null);
        }
    };

    private ArrayAdapter<Channel> channelAdapter;
    private IServiceObserver mObserver = new BaseServiceObserver(){
        @Override
        public void onChannelAdded(Channel channel) throws RemoteException {
            channelAdapter.notifyDataSetChanged();
        }

        @Override
        public void onChannelRemoved(Channel channel) throws RemoteException {
            channelAdapter.notifyDataSetChanged();
        }

        @Override
        public void onChannelUpdated(Channel channel) throws RemoteException {
            channelAdapter.notifyDataSetChanged();
        }
        //dfdf

        @Override
        public void onCurrentChannelChanged() throws RemoteException {
            if (mService.getCurrentChannel().id==0) return;//This is root, the "Public" channel.
            final Intent i = new Intent(ChannelListActivity.this, UserListActivity.class);
            startActivity(i);
        }
    };

    private void updateListView() {

        LayoutInflater inflater = getLayoutInflater();
        ViewGroup header = (ViewGroup)inflater.inflate(R.layout.public_header, (ViewGroup) findViewById(R.id.channelList), false);
        ((ListView) findViewById(R.id.channelList)).addHeaderView(header, null, false);
        findViewById(R.id.faMic).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN){
                    mService.setRecording(true);
                    return true;
                }
                if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL){
                    mService.setRecording(false);
                    return true;
                }
                return false;
            }
        });
        findViewById(R.id.btn_add_channel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try  {
                            mService.addChannel(((TextView)findViewById(R.id.channel_name_text)).getText().toString(), selectableChannels.size());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                thread.start();
            }
        });

        selectableChannels = mService.getChannelList();
        channelAdapter = new ArrayAdapter<Channel>(this, R.layout.channel_list_item, R.id.textView, selectableChannels) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View resView = super.getView(position + 1, convertView, parent);
                resView.findViewById(R.id.buttonJoin).setTag(position + 1);
                resView.findViewById(R.id.buttonJoin).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View v) {
                        Thread thread = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try  {
                                    mService.joinChannel((Integer) v.getTag());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                        thread.start();
                    }
                });
                resView.findViewById(R.id.buttonDel).setTag(position + 1);
                resView.findViewById(R.id.buttonDel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View v) {
                        Thread thread = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try  {
                                    mService.deleteChannel((Integer) v.getTag());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                        thread.start();
                    }
                });
                return resView;
            }

            @Override
            public int getCount() {
                return selectableChannels.size() - 1;
            }
        };
        ((ListView)findViewById(R.id.channelList)).setAdapter(channelAdapter);
    }

    private List<Channel> selectableChannels;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_channel_list);

        Intent intent = new Intent(this, MumbleService.class);
        getApplicationContext().bindService(intent, mServiceConn, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onBackPressed() {
        mService.disconnect();
        mService.unregisterObserver(mObserver);
        ((ListView)findViewById(R.id.channelList)).setAdapter(null);
        super.onBackPressed();
    }
}
